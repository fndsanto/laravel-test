<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\EmployeesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'company'], function() {
   Route::post('create', [CompaniesController::class, 'createCompany']);
   Route::get('list', [CompaniesController::class, 'getCompany']);
   Route::put('edit/{company_id}', [CompaniesController::class, 'updateCompany']);
   Route::delete('delete/{company_id}', [CompaniesController::class, 'deleteCompany']);
});


Route::group(['prefix' => 'employee'], function() {
    Route::post('create', [EmployeesController::class, 'createEmployee']);
    Route::get('list', [EmployeesController::class, 'getEmployee']);
    Route::put('edit/{employee_id}', [EmployeesController::class, 'updateEmployee']);
    Route::delete('delete/{employee_id}', [EmployeesController::class, 'deleteEmployee']);
});
