<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EmployeeTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_employee()
    {
        $response = $this->get('/api/employee/list');

        $response->assertStatus(200);
    }

    public function test_create_employee()
    {
        $response = $this->postJson('/api/employee/create', [
            'firstname' => 'John',
            'lastname' => 'Doe',
            'company' => 1,
            'email' => 'thisisjohn@doe.com',
            'phone' => '321123321'
        ]);

        $response->assertStatus(200)->assertJson([
            'code' => 201,
            'success' => true,
            'message' => 'Employee created!'
        ]);
    }

    public function test_update_employee()
    {
        $response = $this->putJson('/api/employee/edit/1', [
            'firstname' => 'Arial',
            'lastname' => 'Roman',
            'company' => '1',
            'email' => 'john@company.com',
            'phone' => ''
        ]);

        $response->assertStatus(200);
    }

    public function test_delete_employee()
    {
        $response = $this->delete('/api/employee/delete/1');

        $response->assertStatus(200);
    }
}
