<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_company()
    {
        $response = $this->get('/api/company/list');

        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create_company()
    {
        Storage::fake('avatars');

        $file = UploadedFile::fake()->image('avatar.jpg');
        $response = $this->postJson('/api/company/create', [
            'name' => 'JohnCompany',
            'email' => 'john@company.com',
            'logo' => $file,
            'website' => 'https://johnhascompany.com'
        ]);

        $response->assertStatus(200)->assertJson([
            'code' => 201,
            'success' => true,
            'message' => 'Company created!'
        ]);
    }

    public function test_update_company()
    {
        $response = $this->putJson('/api/company/edit/1', [
            'name' => 'JohnCompany',
            'email' => 'john@company.com',
            'logo' => '../../public/image/image.jpg',
            'website' => 'https://johnhascompany.com'
        ]);

        $response->assertStatus(200);
    }

    public function test_delete_company()
    {
        $response = $this->delete('/api/company/delete/1');

        $response->assertStatus(200);
    }
}
