<?php

namespace App\Http\Controllers;

use App\Services\CompaniesService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CompaniesController extends Controller
{
    private $company_service;

    public function __construct(CompaniesService $company_service)
    {
        $this->company_service = $company_service;
    }

    public function createCompany(Request $request)
    {
        if (!isset($request->name))
            return response()->json([
               'code' => 400,
               'success' => false,
               'message' => 'Name required',
               'data' =>  []
            ]);

        $name = $request->name;
        $email = $request->email;
        $logo = $request->logo;
        Storage::put('public/image', $logo);
        $logo = Storage::path('public/image', $logo);
        $website = $request->website;
        $result = $this->company_service->createCompany($name, $email, $logo, $website);

        return response()->json([
            'code' => 201,
            'success' => true,
            'message' => 'Company created!',
            'data' => [
                $result
            ]
        ]);
    }

    public function getCompany()
    {
        $result = $this->company_service->getCompanyPerPage();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Success!',
            'data' => [
                $result
            ]
        ]);
    }

    public function updateCompany(Request $request, $company_id)
    {
        try {
            $company = $this->company_service->getCompanyById($company_id);
            isset($request->name) ? $company->name = $request->name : null;
            if (!isset($company->name))
                return response()->json([
                    'code' => 400,
                    'success' => false,
                    'message' => 'Name required',
                    'data' => []
                ]);

            isset($request->email) ? $company->email = $request->email : null;
            if (isset($request->logo)) {
                $company->logo = $request->logo;
                Storage::put('public/image', $company->logo);
                $company->logo = Storage::path('public/image', $company->logo);
            }
            isset($request->website) ? $company->website = $request->website : null;
            $result = $this->company_service->updateCompany($company);

            return response()->json([
                'code' => 200,
                'success' => true,
                'message' => 'Company updated!',
                'data' => [
                    $result
                ]
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteCompany($company_id)
    {
        try {
            $company = $this->company_service->getCompanyById($company_id);
            $this->company_service->deleteCompany($company);

            return response()->json([
                'code' => 204,
                'success' => true,
                'message' => 'Company deleted successfully!',
                'data' => []
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
