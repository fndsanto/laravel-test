<?php

namespace App\Http\Controllers;

use App\Services\EmployeesService;
use Illuminate\Http\Request;

class EmployeesController extends Controller
{
    private $employees_service;

    public function __construct(EmployeesService $employees_service)
    {
        $this->employees_service = $employees_service;
    }

    public function createEmployee(Request $request)
    {
        if (!isset($request->firstname))
            return response()->json([
                'code' => 400,
                'success' => false,
                'message' => 'Firstname is required',
                'data' =>  []
            ]);

        if (!isset($request->lastname))
            return response()->json([
                'code' => 400,
                'success' => false,
                'message' => 'Lastname is required',
                'data' =>  []
            ]);

        $firstname = $request->firstname;
        $lastname = $request->lastname;
        $company = $request->company;
        $email = $request->email;
        $phone = $request->phone;
        $result = $this->employees_service->createEmployee($firstname, $lastname, $company, $email, $phone);

        return response()->json([
            'code' => 201,
            'success' => true,
            'message' => 'Employee created!',
            'data' => [
                $result
            ]
        ]);
    }

    public function getEmployee()
    {
        $result = $this->employees_service->getEmployeePerPage();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Success!',
            'data' => [
                $result
            ]
        ]);
    }

    public function updateEmployee(Request $request, $employee_id)
    {
        try {
            $employee = $this->employees_service->getEmployeeById($employee_id);
            isset($request->firstname) ? $employee->firstname = $request->firstname : null;
            isset($request->lastname) ? $employee->lastname = $request->lastname : null;

            if (!isset($employee->firstname))
                return response()->json([
                    'code' => 400,
                    'success' => false,
                    'message' => 'Firstname is required',
                    'data' => []
                ]);

            if (!isset($employee->lastname))
                return response()->json([
                    'code' => 400,
                    'success' => false,
                    'message' => 'Lastname is required',
                    'data' => []
                ]);

            isset($request->company) ? $employee->company = $request->company : null;
            isset($request->email) ? $employee->email = $request->email : null;
            isset($request->phone) ? $employee->phone = $request->phone : null;
            $result = $this->employees_service->updateEmployee($employee);

            return response()->json([
                'code' => 200,
                'success' => true,
                'message' => 'Employee updated!',
                'data' => [
                    $result
                ]
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteEmployee($employee_id)
    {
        try {
            $employee = $this->employees_service->getEmployeeById($employee_id);
            $this->employees_service->deleteEmployee($employee);

            return response()->json([
                'code' => 204,
                'success' => true,
                'message' => 'Employee deleted successfully!',
                'data' => []
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
