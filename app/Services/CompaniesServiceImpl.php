<?php

namespace App\Services;

use App\Repositories\CompaniesRepository;

class CompaniesServiceImpl implements CompaniesService
{

    private $companies_repository;

    public function __construct(CompaniesRepository $companies_repository)
    {
        $this->companies_repository = $companies_repository;
    }

    public function createCompany($name, $email, $logo, $website)
    {
        return $this->companies_repository->createCompany($name, $email, $logo, $website);
    }

    public function getCompanyById($company_id)
    {
        $company = $this->companies_repository->getCompanyById($company_id);
        if (isset($company->deleted_at))
            return false;
        else return $company;
    }

    public function getCompanyPerPage()
    {
       return  $this->companies_repository->getCompanyPerPage();
//        foreach ($company as $c) {
//            if (isset($c->deleted_at)){
//
//            }
//        }
    }

    public function updateCompany($company)
    {
        return $this->companies_repository->updateCompany($company);
    }

    public function deleteCompany($company)
    {
        return $this->companies_repository->deleteCompany($company);
    }
}
