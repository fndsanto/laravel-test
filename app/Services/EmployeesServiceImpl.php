<?php

namespace App\Services;

use App\Repositories\EmployeesRepository;

class EmployeesServiceImpl implements EmployeesService
{
    private $employees_repository;

    public function __construct(EmployeesRepository $employees_repository)
    {
        $this->employees_repository = $employees_repository;
    }

    public function createEmployee($firstname, $lastname, $company, $email, $phone)
    {
        return $this->employees_repository->createEmployee($firstname, $lastname, $company, $email, $phone);
    }

    public function getEmployeeById($employee_id)
    {
        $employee = $this->employees_repository->getEmployeeById($employee_id);
        if (isset($employee->deleted_at))
            return false;
        else return $employee;
    }

    public function getEmployeePerPage()
    {
        return  $this->employees_repository->getEmployeePerPage();
    }

    public function updateEmployee($employee)
    {
        return $this->employees_repository->updateEmployee($employee);
    }

    public function deleteEmployee($employee)
    {
        return $this->employees_repository->deleteEmployee($employee);
    }
}
