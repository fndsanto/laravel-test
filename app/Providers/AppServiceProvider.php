<?php

namespace App\Providers;

use App\Repositories\CompaniesRepository;
use App\Repositories\CompaniesRepositoryImpl;
use App\Repositories\EmployeesRepository;
use App\Repositories\EmployeesRepositoryImpl;
use App\Services\CompaniesService;
use App\Services\CompaniesServiceImpl;
use App\Services\EmployeesService;
use App\Services\EmployeesServiceImpl;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(CompaniesRepository::class, CompaniesRepositoryImpl::class);
        $this->app->bind(EmployeesRepository::class, EmployeesRepositoryImpl::class);


        $this->app->bind(CompaniesService::class, CompaniesServiceImpl::class);
        $this->app->bind(EmployeesService::class, EmployeesServiceImpl::class);

    }
}
