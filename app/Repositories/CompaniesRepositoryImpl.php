<?php

namespace App\Repositories;

use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CompaniesRepositoryImpl implements CompaniesRepository
{

    public function createCompany($name, $email, $logo, $website)
    {
        $company = new Company;
        $company->name = $name;
        $company->email = $email;
        $company->logo = $logo;
        $company->website = $website;
        $company->created_at = Carbon::now();
        $company->updated_at = Carbon::now();
        $company->save();

        return $company;
    }

    public function getCompanyById($company_id)
    {
        return DB::table('companies')->where('id', $company_id)->get()->first();
    }

    public function getCompanyPerPage()
    {
        return DB::table('companies')->simplePaginate(10);
    }

    public function updateCompany($company)
    {
        return DB::table('companies')->where('id', $company->id)
            ->update([
                'name' => $company->name,
                'email' => $company->email,
                'logo' => $company->logo,
                'website' => $company->website,
                'updated_at' => Carbon::now()
            ]);
    }

    public function deleteCompany($company)
    {
        return DB::table('companies')->where('id', $company->id)
            ->update([
                'name' => $company->name,
                'email' => $company->email,
                'logo' => $company->logo,
                'website' => $company->website,
                'updated_at' => Carbon::now(),
                'deleted_at' => Carbon::now()
            ]);
    }
}
