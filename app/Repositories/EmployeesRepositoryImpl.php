<?php

namespace App\Repositories;

use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EmployeesRepositoryImpl implements EmployeesRepository
{

    public function createEmployee($firstname, $lastname, $company, $email, $phone)
    {
        $employee = new Employee();
        $employee->firstname = $firstname;
        $employee->lastname = $lastname;
        $employee->company = $company;
        $employee->email = $email;
        $employee->phone = $phone;
        $employee->created_at = Carbon::now();
        $employee->updated_at = Carbon::now();
        $employee->save();

        return $employee;
    }

    public function getEmployeeById($employee_id)
    {
        return DB::table('employees')->where('id', $employee_id)->get()->first();
    }

    public function getEmployeePerPage()
    {
        return DB::table('employees')->simplePaginate(10);

    }

    public function updateEmployee($employee)
    {
        return DB::table('employees')->where('id', $employee->id)
            ->update([
                'firstname' => $employee->firstname,
                'lastname' => $employee->lastname ,
                'company' => $employee->company,
                'email' => $employee->email,
                'phone' => $employee->phone,
                'updated_at' => Carbon::now()
            ]);
    }

    public function deleteEmployee($employee)
    {
        return DB::table('employees')->where('id', $employee->id)
            ->update([
                'firstname' => $employee->firstname,
                'lastname' => $employee->lastname ,
                'company' => $employee->company,
                'email' => $employee->email,
                'phone' => $employee->phone,
                'updated_at' => Carbon::now(),
                'deleted_at' => Carbon::now()
            ]);
    }
}
