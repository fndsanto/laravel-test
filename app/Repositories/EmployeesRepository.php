<?php

namespace App\Repositories;

interface EmployeesRepository
{
    public function createEmployee($firstname, $lastname, $company, $email, $phone);
    public function getEmployeeById($employee_id);
    public function getEmployeePerPage();
    public function updateEmployee($employee);
    public function deleteEmployee($employee);
}
