<?php

namespace App\Repositories;

interface CompaniesRepository
{
    public function createCompany($name, $email, $logo, $website);
    public function getCompanyById($company_id);
    public function getCompanyPerPage();
    public function updateCompany($company);
    public function deleteCompany($company);
}
